-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2021 at 07:58 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mtp-sarah`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `nohp` int(12) NOT NULL,
  `alamat` tinytext NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `no_antrian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama_lengkap`, `nohp`, `alamat`, `username`, `password`, `no_antrian`) VALUES
(23, 'User5', 0, 'Indonesia', 'user5', 'user5', 7),
(24, 'User4', 0, 'Indonesia', 'user4', 'user4', 2),
(25, 'User3', 0, 'Indonesia', 'user3', 'user3', 3),
(26, 'User2', 0, 'Indonesia', 'user2', 'user2\r\n', 5),
(27, 'User1', 0, 'Indonesia', 'user1', 'user1', 6),
(28, 'Sarah Indriani', 888, 'Bima', 'sarah', '$2y$10$XB7B0k5vrIXRrLEYpoiFkubRI93bkaNKk5Duwj3rQYmxoQLoc6cGK', 0),
(29, 'Shella Rossa Sukmawati', 8888, 'Yogyakarta', 'shella', '$2y$10$vejjdCeWGfST4hLZJKup1ei.PIHCqHmyp4ko7AaBbgnidsnMmt6oG', 8),
(30, 'Irma Eryanti Putri', 8888, 'Yogyakarta', 'irma', '$2y$10$4s0HkIOW/BwBRhncdDAKyOiuhsAFMyA6VKrWQURAyxfJdXHOSTFTO', 0),
(31, 'M Imamul Mukminin', 8888, 'Bima', 'imam', '$2y$10$m1NEVV7y6eIvhktXfICfNOcKeY51JnrSJ2yUkgPQiHkaCIExA0qnm', 0),
(32, 'Muhammad Wahyu Aditya S', 8888, 'Yogyakarta', 'wahyu', '$2y$10$GMiACiG7AI7DgnBACG8IWOzuwqjS8IYNit6Y.EGaAKyZU6kNmYc4S', 0),
(33, 'Muhammad Fajri Majid', 8888, 'Yogyakarta', 'fajar', '$2y$10$gVYc8KNH8UhZZ7t5h69XnetHs6a8tQ1ezPk.6x/yLENdwJbnJ4L2m', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
