<?php

require 'connect.php';

//  QUERY UNTUK MENDAPATKAN SEMUA USER BERDASARKAN NOMOR ANTRIAN YANG PALING KECIL
$sql = "SELECT nama_lengkap, no_antrian FROM users ORDER BY no_antrian ASC";

$result = $conn->query($sql);
$users = $result->fetch_all();

echo json_encode($users);
