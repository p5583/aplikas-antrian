<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Beranda</title>
</head>

<body>

    <div class="d-flex flex-column align-items-center justify-content-center gap-2 text-white bg-primary vh-100">

        <h1 class="fw-bold">Selamat Datang di Toko HANZBOY</h1>
        <h3>Booking Antrian</h3>
        <div class="d-flex gap-3">
            <a href="login.php" class="btn btn-success text-white btn-lg">Login</a>
            <a href="register.php" class="btn btn-warning text-white btn-lg">Register</a>
        </div>
    </div>

</body>

</html>