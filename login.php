<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/login.css">

    <!-- JAVASCRIPT -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <title>Login</title>
</head>

<body>

    <div class="login d-flex justify-content-center align-items-center flex-column">

        <!-- CARD LOGIN -->
        <div class="card col-3 p-4">
            <h2 class="text-center text-primary mb-4 fw-bold">Login</h2>

            <!-- TOAST -->
            <div class="toast align-items-center text-white bg-danger border-0 mb-3" role="alert" aria-live="" aria-atomic="true">
                <div class="d-flex">
                    <div class="toast-body">
                        Username atau Password salah!
                    </div>
                    <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>

            <!-- USERNAME INPUT -->
            <label for="username" class="form-label">Username</label>
            <input type="text" class="username form-control mb-3" id="username" name="username" required>

            <!-- PASSWORD INPUT -->
            <label for="password" class="form-label">Password</label>
            <input type="password" class="password form-control mb-4" id="password" name="password" required>

            <div class="d-grid gap-2">
                <button type="button" class="btn btn-primary me-1 mb-3">Login</button>
            </div>
            <p class="text-register text-secondary">Belum punya akun? Daftar <a href="register.php" class="text-primary text-decoration-none fw-bold">disini</a></p>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script>
        var toastEl = document.querySelector('.toast');
        var toast = new bootstrap.Toast(toastEl);
        // Menyembunyikan toast bootstrap
        $('.toast').hide();

        // FUNGSI BUTTON UNTUK MELAKUKAN LOGIN
        $('button').click(function() {
            $.ajax({
                type: 'POST',
                url: 'validasi.php',
                data: {
                    username: $('.username').val(),
                    password: $('.password').val()
                },
                success: function(result) {
                    if (!result) {
                        $('.toast').show();
                        toast.show();
                        setTimeout(function() {
                            $('.toast').hide();
                        }, 6000);
                        $('.username').addClass("is-invalid");
                        $('.password').addClass("is-invalid");
                    } else
                        window.location.replace("antrian.php");

                }

            })
        })
    </script>

</body>

</html>