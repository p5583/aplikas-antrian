<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->
    <link rel="stylesheet" href="css/register.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">

    <!-- JAVASCRIPT -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <title>Register</title>
</head>

<body>

    <div class="register d-flex justify-content-center align-items-center">
        <div class="card col-4 p-4">
            <h2 class="text-primary mb-4 fw-bold">Daftar Akun</h2>

            <!-- USERNAME -->
            <label for="nama" class="form-label">Nama Lengkap</label>
            <input type="text" class="nama form-control mb-3" id="nama" name="nama" required>

            <!-- NOMOR TELEPON -->
            <label for="nohp" class="form-label">Nomor Telepon</label>
            <input type="text" class="no-hp form-control mb-3" id="nohp" name="nohp" required>

            <!-- ALAMAT -->
            <div class="form-floating mb-3">
                <textarea class="alamat form-control" placeholder="Leave a comment here" id="alamat" style="height: 100px" name="alamat" required></textarea>
                <label for="alamat">Alamat</label>
            </div>

            <!-- USERNAME -->
            <label for="username" class="form-label">Username</label>
            <input type="text" class="username form-control mb-3" id="username" name="username" required>

            <!-- PASSWORD -->
            <label for="password" class="form-label">Password</label>
            <input type="password" class="password form-control mb-3" id="password" name="password" required>

            <div class="d-grid gap-2">
                <button type="button" class="btn btn-primary me-1 mb-3">Daftar</button>
            </div>
            <a href="login.php" class="back text-decoration-none"><i class="fas fa-arrow-left fa-sm"></i> Kembali ke login</a>
        </div>
    </div>

    <script>
        // FUNGSI BUTTON UNTUK REGISTER AKUN
        $('button').click(function() {

            $.ajax({
                type: 'POST',
                url: "proses-register.php",
                data: {
                    nama: $('.nama').val(),
                    noHp: $('.no-hp').val(),
                    alamat: $('.alamat').val(),
                    username: $('.username').val(),
                    password: $('.password').val(),
                },
                success: function(result) {
                    if (result) {
                        setTimeout(function() {
                            window.location.replace("login.php");
                        }, 4000);

                        Swal.fire({
                            title: 'Akun berhasil didaftarkan!',
                            icon: 'success',
                            confirmButtonText: 'Kembali login'
                        });

                    } else {
                        Swal.fire({
                            title: 'Register gagal!',
                            text: "Pastikan mengisi form dengan benar",
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                }
            });

        });
    </script>

</body>

</html>