<?php

require 'connect.php';

session_start();

if (!isset($_SESSION['user'])) {
    header("location: login.php");
}

$id = $_SESSION['user'];

$sql = "SELECT * FROM users WHERE id = '$id'";
$result = $conn->query($sql);

$row = $result->fetch_assoc();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/antrian.css">

    <!-- JAVASCRIPT -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <title>Beranda</title>
</head>

<body class="bg-light">

    <nav class="navbar text-white py-3">
        <div class="container">

            <div class="d-flex gap-5 align-items-center">
                <h4 class="m-0">Toko HANZBOY</h4>
            </div>
            <a href="logout.php" class="nav-item text-white text-decoration-none">Logout <i class="fas fa-sign-out-alt"></i></a>
        </div>
    </nav>

    <div class="container mt-4 d-flex align-items-start justify-content-around gap-4">
        <div class="col-8 d-flex flex-column">

            <div class="bg-white card">
                <h2 class="antrian-saat-ini p-3">Nomor antrian saat ini</h2>

                <!-- NOMOR ANTRIAN SAAT INI -->
                <h1 class="queue-now display-1 text-center fw-bold mt-5"></h1>

                <div class="alert alert-info mt-5 mb-4 mx-4" role="alert">
                    <i class="fas fa-info-circle fa-lg"></i>
                    <span>Belum memulai antrian!</span>
                </div>
            </div>

            <!-- ANTRIAN BERIKUTNYA -->
            <div class="bg-white mt-4 p-4">
                <ul class="list-group">
                    <li class="list-group-item">
                        <h2>Antrian berikutnya</h2>
                    </li>
                </ul>
            </div>
        </div>


        <!-- ANTRIAN ANDA -->
        <div class="col-4 bg-white card">
            <h2 class="label-antrian-anda card-title p-3">Antrian Anda</h2>

            <input type="hidden" value="<?= $row['id']; ?>" class="id">

            <div class="px-3 pt-2">
                <p class="my-2">Nama : <?= $row['nama_lengkap']; ?></p>
                <p class="my-2">No Telepon : <?= $row['nohp']; ?></p>
                <p class="my-2">Alamat : <?= $row['alamat']; ?></p>

                <h5 class="text-secondary border-top mt-4 pt-3">No antrian :</h5>

                <?php if ($row['no_antrian'] == 0) : ?>

                    <div class="antrian-tidak-ada text-center m-4">
                        <h4 class="text-secondary text-center">Tidak ada nomor antrian!</h4>
                        <button class="btn btn-danger mt-3">Ambil Antrian</button>
                    </div>

                <?php endif; ?>
                <h1 class="nomor-antrian-anda display-3 text-center fw-bold"></h1>
            </div>
        </div>
    </div>

    <div class="toast bg-success text-white position-absolute bottom-0 end-0 m-4" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body p-3">
            <i class="far fa-check-circle me-2"></i>
            Berhasil mendapatkan nomor antrian!
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script>
        var toastEl = document.querySelector('.toast');
        var toast = new bootstrap.Toast(toastEl);

        let queue1 = [];

        // MELOAD ANTRIAN
        function loadQueue() {
            $.ajax({
                url: 'getAllUsers.php',
                success: function ucup(result) {
                    if (result != 0) {
                        var queue = JSON.parse(result);
                        queueList(queue);
                    } else {
                        alert("Queue list gagal!");
                    }
                }
            });
        }

        loadQueue();

        //  FUNCTION BUTTON UNTUK MENDAPATKAN NOMOR ANTRIAN
        $('button').click(function(params) {
            $.ajax({
                type: 'POST',
                url: 'getNumber.php',
                data: {
                    id: $('.id').val()
                },
                success: function(result) {
                    if (result != 0) {
                        toast.show();
                        $('.nomor-antrian-anda').show();
                        $('.nomor-antrian-anda').text(result);
                        $('.antrian-tidak-ada').hide();
                        queue1.push(result);
                        $('.list-group').append('<li class="list-group-item">' + result + '</li>');
                        mulaiAntrian();
                    } else {
                        alert("gagal mengambil nomor antrian");
                    }
                }
            })
        });

        // ANTRIAN ANDA
        $.ajax({
            type: 'POST',
            url: 'antrianAnda.php',
            data: {
                id: $('.id').val()
            },
            success: function(result) {
                if (result != 0) {
                    var antrianAnda = JSON.parse(result);

                    if (antrianAnda.no_antrian == 0) {
                        $('.nomor-antrian-anda').hide();
                    }

                    $('.nomor-antrian-anda').text(antrianAnda.no_antrian);

                } else {
                    alert("gagal memuat data anda");
                }
            }
        });

        // LIST QUEUE
        function queueList(queue) {
            var i = 0;
            while (i < queue.length) {
                if (queue[i][1] != 0) {
                    $('.list-group').append('<li class="list-group-item">' + queue[i][1] + '</li>');
                    queue1.push(queue[i][1]);
                }
                i++;
            }
        }

        // MEMULAI ANTRIAN
        function mulaiAntrian() {
            var looper = setInterval(function() {
                $('.queue-now').text(queue1[0]);
                $('.alert span').text("Antrian dengan nomor " + queue1[0] + " harap segera ke toko!");

                if (queue1 == 0) {
                    $('.alert span').text("Antrian telah selesai!");

                    clearInterval(looper);
                }

                queue1.shift();
                $('.list-group').find('li:nth-child(2)').remove();

            }, 6000);
        }
        mulaiAntrian();
    </script>
</body>

</html>