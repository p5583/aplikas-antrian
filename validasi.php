<?php

require 'connect.php';

session_start();

$username = $_POST['username'];
$password = $_POST['password'];

// QUERY UNTUK MENGECEK USERNAME
$sql = "SELECT * FROM users WHERE username='$username'";
$result = $conn->query($sql);

// VALIDASI UNTUK MENGECEK PASSWORD
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    $hash = $row['password'];

    if (password_verify($password, $hash)) {
        $_SESSION['user'] = $row['id'];
        echo true;
    } else
        echo false;
} else
    echo false;
