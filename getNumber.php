<?php

require 'connect.php';

session_start();
$id = $_POST['id'];

//  SEBELUM
function antrianMax()
{
    require_once 'connect.php';

    $sql = "SELECT MAX(no_antrian) FROM users";

    $result = $conn->query($sql);

    $row = $result->fetch_assoc();

    $numberQueue = $row['MAX(no_antrian)'];
    ++$numberQueue;

    //  MENAMBAHKAN NOMOR ANTRIAN , JIKA NILAI MAKSIMALNYA ADALAH 6 MAKA ANTRIANNYA ADALAH 7 KARNA NILAI MAX + 1
    $update = "UPDATE users SET no_antrian = '$numberQueue' WHERE id = '$id'";

    if ($conn->query($update)) {
        echo $numberQueue;
    } else {
        echo false;
    }
}

//  SETELAH
function antrianMax()
{
    require_once 'connect.php';

    $sql = "SELECT MAX(no_antrian) FROM users";

    $result = $conn->query($sql);

    $row = $result->fetch_assoc();

    $numberQueue = $row['MAX(no_antrian)'];
    ++$numberQueue;

    updateAntrian($numberQueue);
}

function updateAntrian($numberQueue)
{
    //  MENAMBAHKAN NOMOR ANTRIAN , JIKA NILAI MAKSIMALNYA ADALAH 6 MAKA ANTRIANNYA ADALAH 7 KARNA NILAI MAX + 1
    $update = "UPDATE users SET no_antrian = '$numberQueue' WHERE id = '$id'";

    if ($conn->query($update)) {
        echo $numberQueue;
    } else {
        echo false;
    }
}


// MENCARI NILAI MAKSIMAL NOMOR ANTRIAN
$sql = "SELECT MAX(no_antrian) FROM users";

$result = $conn->query($sql);

$row = $result->fetch_assoc();

$numberQueue = $row['MAX(no_antrian)'];
++$numberQueue;

//  MENAMBAHKAN NOMOR ANTRIAN , JIKA NILAI MAKSIMALNYA ADALAH 6 MAKA ANTRIANNYA ADALAH 7 KARNA NILAI MAX + 1
$update = "UPDATE users SET no_antrian = '$numberQueue' WHERE id = '$id'";

if ($conn->query($update)) {
    echo $numberQueue;
} else {
    echo false;
}
